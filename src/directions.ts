export const enum Directions {
    undefined = 0,
    left = -1,
    right = 1
}